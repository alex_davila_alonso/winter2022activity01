public class AreaComputations{
	//will do side^2 to find square area
	public static double areaSquare(double side){
		double area = side*side;
		return area;
	}
	//will do length x width to find the rectangle area
	public double areaRectangle(double length, double width){
		double area = length*width;
		return area;
	}
}