public class MethodsTest{
	public static void main(String args[]){
		int x = 10;
		double y = 5.0;
		
		//Part 2 4.
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x); 
		
		//Part 2 5.
	    methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50); 
		
		//Part 2 6.
		methodTwoInputNoReturn(x,y);
		
		//Part 2 7.
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		//Part 2 8.
		double square = sumSquareRoot(6,3);
		System.out.println(square);
		
		//part 2 10.
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		//Part 2 11.
		int num = SecondClass.addOne(50);
		System.out.println(num);
		SecondClass sc = new SecondClass();
		int num2 = sc.addTwo(50);
		System.out.println(num2);
	}
	
	//Method for Part 2 4.
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x  = 50;
		System.out.println(x);
	}
	
	//Method for Part 2 5.
	public static void methodOneInputNoReturn(int x){
		System.out.println(x);
	}
	
	//Method for part 2 6.
	public static void methodTwoInputNoReturn(int x, double y){
		System.out.println(x);
		System.out.println(y);
	}
	
	//Method for Part 2 7.
	public static int methodNoInputReturnInt(){
		return 6;
	}
	
	//Method for Part 2 8.
	public static double sumSquareRoot(int x, int b){
		double num = x + b;
		double square = Math.sqrt(num);
		return square;
	}
}