import java.util.Scanner;
public class PartThree{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		
		//input for areaSquare method
		System.out.println("Enter the side of the square");
		double side = input.nextDouble();
		
		//input for areaRectangle method
		System.out.println("Enter the length of the rectangle");
		double length = input.nextDouble();
		System.out.println("Enter the width of the rectangle");
		double width = input.nextDouble();
		
		//calling the methods from the AreaComputations class
		double square = AreaComputations.areaSquare(side);
		AreaComputations area = new AreaComputations();
		double rectangle = area.areaRectangle(length,width);
		
		System.out.println("your square area is: "+square);
		System.out.println("your rectangle area is: "+rectangle);
	}
}